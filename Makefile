####################################################################
# Distribution Makefile
####################################################################

.PHONY: configure install clean rpm srpm sources

all: configure man

####################################################################
# Configure
####################################################################

# general setup
CC	 = gcc
CC_FLAGS = -Wall -O -fpic -ggdb
CXX	 = g++


BINFILES = wassh wassh-decorate wassh-files
CFGFILES = etc/wassh.conf
SSMFILES = $(wildcard ssm-cern/*.pm)

PARALLELDOC = parallel.sgml
SPECDIR = $(shell rpm --eval '%_specdir')
SOURCEDIR = $(shell rpm --eval '%_sourcedir')
RPMVERSION = $(shell grep '^Version' wassh.spec | cut -f 2 -d ' ')
TARFILE = $(SOURCEDIR)/wassh-$(RPMVERSION).tar.gz
SPECFILE = $(SPECDIR)/wassh.spec

MANSECT = 8
MAN8DIR = /usr/share/man/man$(MANSECT)

ifdef DISTTAG
RPMDIST=--define '%dist $(DISTTAG)'
else
RPMDIST=
endif

configure: $(BINFILES) $(CFGFILES) $(SSMFILES)

####################################################################
# Man pages
####################################################################

man: configure ssh-probe
	@echo creating man pages ...
	for i in $(BINFILES) ; do \
           pod2man $(_podopt) $$i > \
                   $$i.$(MANSECT); \
        done
	for i in $(SSMFILES) ; do \
           pod2man $(_podopt) $$i > \
                   wassh-ssm-`basename $$i .pm`.$(MANSECT)pm; \
        done

	docbook2man $(PARALLELDOC)  # this causes '(invalid location)'. result looks OK..

	gzip -f *.$(MANSECT) *.$(MANSECT)pm



####################################################################
# Install
####################################################################

install: configure man
	@echo installing ...
	@mkdir -p $(PREFIX)/usr/bin
	@mkdir -p $(PREFIX)/usr/libexec
	@mkdir -p $(PREFIX)/etc/wassh
	@mkdir -p $(PREFIX)/$(MAN8DIR)
	@mkdir -p $(PREFIX)/usr/lib/perl/wassh_ssm

	install -m 0555 wassh $(PREFIX)/usr/bin/wassh
	install -m 0555 wassh-decorate $(PREFIX)/usr/libexec/wassh-decorate
	install -m 0555 wassh-files $(PREFIX)/usr/libexec/wassh-files
	install -m 0555 parallel $(PREFIX)/usr/libexec/parallel
	install -m 0555 ssh-probe $(PREFIX)/usr/libexec/ssh-probe
	install -m 0644 etc/wassh.conf $(PREFIX)/etc/wassh/wassh.conf
	install -m 0444 *.$(MANSECT).gz $(PREFIX)/$(MAN8DIR)/

	install -m 0555 $(SSMFILES) $(PREFIX)/usr/lib/perl/wassh_ssm/
	install -m 0555 *.$(MANSECT)pm.gz $(PREFIX)/$(MAN8DIR)/

	@echo installation complete ...

####################################################################

clean::
	@echo cleaning files ...
	@rm -f *.$(MANSECT).gz *.$(MANSECT)pm.gz
	@rm -f ssh-probe
	@rm -f manpage.*
	@rm -f config.sh 
	@rm -f  *.~	
	@rm -f  *~
	@rm -f  *.tgz

ssh-probe: ssh-probe.c
	$(CC) $(CC_FLAGS) -o $@ ssh-probe.c

$(TARFILE):: clean
	sed -i -e "s/^\([ \t]*.self->{'VERSION'} =\).*/\1 '$(RPMVERSION)';/" wassh wassh-decorate wassh-files
	tar czvf $(SOURCEDIR)/wassh-$(RPMVERSION).tar.gz  --exclude-vcs --transform="s:wassh:wassh-$(RPMVERSION):" ../wassh/

$(SPECFILE)::
	cp -v wassh.spec $(SPECDIR)

srpm: $(TARFILE) $(SPECFILE)
	rpmbuild -bs $(RPMDIST)  $(SPECDIR)/wassh.spec

rpm: $(TARFILE) $(SPECFILE)
	rpmbuild -bb $(RPMDIST)  $(SPECDIR)/wassh.spec

sources: $(TARFILE)
	cp -v $(SOURCEDIR)/wassh-$(RPMVERSION).tar.gz .
