COMP=wassh
NAME=$(COMP)
DESCR=pluggable parallel SSH Client
VERSION=3.0.2
RELEASE=5
AUTHOR=Rohitashva Sharma <Rohitashva.Sharma@barc.gov.in>
MAINTAINER=Jan Iven <jan.iven@cern.ch>

WASSH_SSM_NS=wassh_ssm
WASSH_SSM_DIR=$(QTTR_PERLLIB)/$(WASSH_SSM_NS)

MANSECT=8

MAN8DIR=$(QTTR_MAN)/man$(MANSECT)

DATE=19/11/08 18:17

TARFILE=wassh-3.0.2.src.tgz
PROD=\#
