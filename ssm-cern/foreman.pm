package wassh_ssm::foreman;

use Exporter;

use JSON;
use Carp;
use Data::Dumper;
use Net::Domain qw(hostdomain);
use URI::Escape qw(uri_escape);

@ISA = qw(Exporter);

@EXPORT = qw(expandHosts);

sub expandHosts {
	my $attribs     = $_[0];
	my @ret         = ();
	my $debug = $::this_app->option('debug');
	my $verbose = $::this_app->option('verbose');


	#need to build up one giantic search string

	foreach my $unsupported ('location_room','rackname' ) {
	    if (defined $$attribs{$unsupported} ) { $::this_app->warn('PUPPET: search by '.$unsupported.' is not supported');}
	}
	
	if (defined $$attribs{'allnodes'}){
	    $puppetstring = 'name ~ *';
	} else {
	    my @query;
	    if (defined $$attribs{'hostname'} ) {
		push (@query, puppet_search($$attribs{'hostname'},'name'));
	    }
	    if (defined $$attribs{'operatingsystem'} ) {
		push (@query, puppet_search($$attribs{'operatingsystem'},'operatingsystem'));
	    }
	    if (defined $$attribs{'architecture'} ) {
		push (@query, puppet_search($$attribs{'architecture'},'architecture'));
	    }
	    if (defined $$attribs{'clustername'} ) {
		push (@query, puppet_search($$attribs{'clustername'},'cluster'));
	    }
	    if (defined $$attribs{'type'} ) {
		push (@query, puppet_search($$attribs{'type'},'class'));
	    }
	    if (defined $$attribs{'stage'} ) {
		push (@query, puppet_search($$attribs{'stage'},'stage'));
	    }
	    $puppetstring = join(' AND ',@query);
	}

        my $escaped_puppetstring = uri_escape($puppetstring);
	my $curlverbose = ($debug ? "--verbose" : "--silent --show-error"); # is too detailed for "verbose"
	my $foreman = $ENV{'FOREMAN_URL'};
	$foreman = "https://judy-simple.cern.ch:8443" unless $foreman;
	my $curlcommand = "curl $curlverbose --insecure --location --request GET --header 'Content-Type:application/json' --header 'Accept:application/json, version=2' --negotiate --user : --url '$foreman/api/hosts/?per_page=10000000&search=$escaped_puppetstring'";

	$::this_app->debug(1,'SSM::Foreman: searching via '. $curlcommand);
	
	my $reply  = `$curlcommand`;
	if($? == 0) {
	    $::this_app->debug(2,"SSM::Foreman: got '$reply'");
	    if ($reply =~ /^\s*\[\s*\{/ ) { # technically, there could be other valid JSON syntaxes
		my $json_raw = from_json($reply);
		$::this_app->debug(3,Dumper($json_raw));
		my @json = @{$json_raw};         # this assumes the above "set of"-smething
		for my $entry (@json) {
		    #print Dumper($entry);
		    my $host =  $entry->{'host'}->{'name'};
		    push(@ret, $host);
		}
	    } elsif ($reply eq '' || $reply eq '[]') {
		$::this_app->verbose("SSM::Foreman: no data found");
	    } elsif ($reply =~ '^\s*{' ) {
		my %data = %{from_json($reply)};
		$::this_app->debug(3,Dumper(\%data));
		for my $entry (@{$data{results}}){
		    my $host = $entry->{'name'};
		    push(@ret, $host);
		}
	    } else {
		$::this_app->warn("SSM::Foreman: Error getting Foreman data for $puppetstring - Foreman said:\n".$reply);
	    }
	} else {
	    $::this_app->warn("SSM::Foreman: Error getting Foreman data for $puppetstring : ".$reply);
	}

	$::this_app->debug(1,'SSM::Foreman: returned '.join(',',@ret));
	return (1, "Success", @ret);
}

sub puppet_search($$) {
    my ($selectors, $type) = @_;
    my @ret;

    foreach my $sel (split(/ +|,+/, $selectors)) {
	# quirks:
	## names with wildcards need to end in the domain, otherwise will not be found
	## operating system needs to be split into "os" and "os_major" , and uppercased?
	## missing 'subcluster' finds all on CDB, none on Puppet
	if ($type eq 'name' and $sel =~ /\%|\*/ and not $sel =~ /\./) {
	    $sel .= hostdomain();
	    push(@ret, $type .' ~ '.$sel);
	} elsif ($type eq 'operatingsystem' ) {
	    $sel =~ s/rhel/RedHat/i;
	    $sel =~ s/slc/SLC/i;
	    if($sel =~ /^([*[:alpha:]]+)(\d+)/) {   #slc5, rhel6, s*5
		push(@ret, 'os_major = '.$2.' AND os ~ '.$1);
	    } elsif($sel =~ /^([*[:alpha:]]+)/){    # slc, rhel
		push(@ret, 'os ~ '.$1);
	    }  elsif($sel =~ /(\d+)$/) {            # *6
		push(@ret, 'os_major = '.$1);
	    } else {
		$::this_app->warn("SSM::Foreman: unlikely to have understood $type = $sel");
	    }
	} elsif ( $type eq 'cluster' ) {
	    if ($sel =~ m:^([^/]+)/([^/]+)$: ) {  # cluster/subcluster notation

		push(@ret, "(( params.cluster ~ $1 AND params.subcluster ~ $2 ) OR ( hostgroup_fullname ~ $sel ))");  
	    } elsif ($sel =~ m:^([^/]+)$:) {  # just cluster, but with wildcards
		push(@ret, "(( params.cluster ~ $1 ) OR ( hostgroup_fullname ~ $sel ))");
	    } else {
		if ($sel =~ m/[^\*]\*[^\*]/) { # wildcard in the middle means Foreman is _not_ going to pad with wildcards - unlike normally
		    $sel = '*'.$sel.'*';
		}
		push(@ret, 'hostgroup_fullname ~ '.$sel)	
	    }
	} elsif ( $type eq 'stage' ) {
	    if ( $sel eq 'prod' and $sel eq $selectors) {
		push(@ret, 'params.alarmed = true');
	    } elsif ( $sel eq 'preprod' or  $sel eq 'test' ) {
		push(@ret, 'NOT params.alarmed');  # not always set, missing does not match "false"
	    } else {
		$::this_app->warn("SSM::Foreman: stage = $selectors , don't know whether to look for alarmed machines or not");
	    }
	} else {
	    push(@ret,  $type .' ~ '.$sel);
	}
    }
    return '( '.join(' OR ', @ret).' )';
}

1;

=pod 

=head1 Description

This SSM connects to the Foreman API via B<curl> (with Kerberos
authentication) and retrieves lists of hosts. Translates the various
query attributes into something that lookes like it might fit on
Foreman.

=over

=item I<cluster> becomes I<hostgroup> (pattern match)

as a special case, will also look for foreman parameters I<cluster> and
  I<subcluster> in case the provided I<cluster> has the form
  C<somestring/someotherstring>.

=item I<stage=prod> becomes I<params.alarmed=true>

=back

=cut

=head1 Known Bugs

=over

=item only machines visible to the current user in Foreman will be
  returned - at variance with CDBSQL, where all machines would be
  listed.

=item may not handle Foreman errors gracefully (assumes anything
  coming back is JSON)

=item hardcoded to use https://judy.cern.ch:8043

=item wildcard handling in Foreman is non-intuitive - strings without
  wildcard will match anywhere in the hostgroup name, strings with a
  wildcard are anchored at beginning and end. This SSM might match too
  many hosts.

=back

=cut

=head1 SEE ALSO

wassh

=head1 AUTHOR

Jan Iven
