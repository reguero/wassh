package wassh_ssm::puppetdb;

use Exporter;

use JSON;
use Carp;
use Data::Dumper;
use Net::Domain qw(hostdomain);
use URI::Escape;

@ISA = qw(Exporter);

@EXPORT = qw(expandHosts);

my $PUPPETDB_URL = 'https://constable-simple.cern.ch:9081';

sub expandHosts {
	my $attribs     = $_[0];
	my @ret         = ();
	my $debug = $::this_app->option('debug');
	my $verbose = $::this_app->option('verbose');

	my $puppetdb = $ENV{'PUPPETDB_URL'};
	$puppetdb = $PUPPETDB_URL unless $puppetdb;
	$puppetstring='';

	if (! defined $$attribs{'allnodes'} ) {  # empty query = get all..

	  my @query;
	  if (defined $$attribs{'hostname'} ) {
	    push (@query, puppetdb_search($$attribs{'hostname'},'name'));
	  }
	  if (defined $$attribs{'rackname'} ) {
	    push (@query, puppetdb_search($$attribs{'rackname'},'rack'));
	  }
	  if (defined $$attribs{'location_room'} ) {
	    push (@query, puppetdb_search($$attribs{'location_room'},'room'));
	  }
	  if (defined $$attribs{'operatingsystem'} ) {
	    push (@query, puppetdb_search($$attribs{'operatingsystem'},'operatingsystem'));
	  }
	  if (defined $$attribs{'architecture'} ) {
	    push (@query, puppetdb_search($$attribs{'architecture'},'architecture'));
	  }
	  if (defined $$attribs{'clustername'} ) {
	    push (@query, puppetdb_search($$attribs{'clustername'},'cluster'));
	  }
	  if (defined $$attribs{'stage'} ) {
	    push (@query, puppetdb_search($$attribs{'stage'},'stage'));
	  }
	  if (defined $$attribs{'clustertype'} ) {
	    # abuse this for generic queries for a fact=value
	    if($$attribs{'clustertype'} =~ m/=/) {
	      my ($fact,$value) = split('=', $$attribs{'clustertype'});
	      $::this_app->debug(1, 'SSM::PuppetDB: will perform generic query for "$fact"~"$value"');
	      push (@query, puppetdb_search($value,$fact));
	    } else {
	      $::this_app->verbose('SSM::PuppetDB: ignoring argument type='.$$attribs{'clustertype'}.', expect fact=value pair');
	    }
	  }
	  if($#query >0) {
	    $puppetstring = '["and",'.join(',',@query).']'."\n";
	  } else {
	    $puppetstring = $query[0];
	  }
	  if( ! $puppetstring) {
	    $::this_app->warn("SSM::PuppetDB: empty query string, bailing out - use --allnodes to get all nodes");
	    return (-1, "empty query string",[]);
	  }
	}

	my $curlverbose = (($debug  and $debug > 2) ? "--verbose" : "--silent --show-error"); # is too detailed for "verbose"
	my $curlcommand = "curl $curlverbose --insecure --location --request GET --header 'Content-Type:application/json' --header 'Accept:application/json, version=2' --negotiate --user :  -G '$puppetdb//v4/facts' --data-urlencode 'query=$puppetstring' --data-urlencode 'limit=100000' ";

	  $::this_app->debug(1,'SSM::PuppetDB: searching via '. $curlcommand);

	  my $reply  = `$curlcommand`;
	  if($? == 0) {
	    $::this_app->debug(2,"SSM::PuppetDB: got '$reply'");
	    if ($reply =~ /^\s*\[\s*\{/ ) { # technically, there could be other valid JSON syntaxes
	      my $json_raw = from_json($reply);
	      $::this_app->debug(3,Dumper($json_raw));
	      my @json = @{$json_raw};         # this assumes the above "set of"-something
	      for my $entry (@json) {
                $::this_app->debug(5, Dumper($entry));
		my $host =  $entry->{'certname'};
		push(@ret, $host);
	      }
	    } elsif ($reply eq '' || $reply=~ m/^\[ *\]$/) {
	      $::this_app->verbose("SSM::PuppetDB: no data found");
	    } elsif ($reply =~ '^\s*{' ) {
	      my $json_raw = from_json($reply);
	      $::this_app->debug(3,Dumper($json_raw));
	      if (defined($json_raw{'message'})) {
		$::this_app->warn("SSM::PuppetDB: Error getting Puppetdb data for $puppetstring : ".$json_raw{'message'});
		} else {
		  $::this_app->warn("SSM::PuppetDB: Error getting Puppetdb data for $puppetstring : ".$reply); 
		}
	    } else {
	      $::this_app->warn("SSM::PuppetDB: Error getting Puppetdb data for $puppetstring - Puppetdb said:\n".$reply);
	    }
	  } else {
	    $::this_app->warn("SSM::PuppetDB: Error getting Puppetdb data for $puppetstring : ".$reply);
	  }
	
	$::this_app->debug(1,'SSM::PuppetDB: returned '.join(',',@ret));
	return (1, "Success", @ret);
}

sub puppetdb_search($$) {
    my ($selectors, $type) = @_;
    my @ret;

    foreach my $sel (split(/ +|,+/, $selectors)) {
        $sel =~ s/\*/.*/g; # go from shell-style glob to regexps

	# quirks:
	## names with wildcards need to end in the domain, otherwise will not be found
	## operating system needs to be split into "os" and "os_major" , and uppercased?
	## missing 'subcluster' finds all on CDB, none on Puppet
	if ($type eq 'name' and $sel =~ /\%|\*/ and not $sel =~ /\./) {
	    $sel .= hostdomain();
	    push(@ret, '["~","name","'.$sel.'"]');
	} elsif ($type eq 'operatingsystem' ) {
	    $sel =~ s/rhel/RedHat/i;
	    $sel =~ s/slc/SLC/i;
	    if($sel =~ /^([*[:alpha:]]+)(\d+.*)/) {   #slc5, rhel6, s*5
		push(@ret, '["and",'.
		     '["and",["=","name","operatingsystemrelease"],["~","value","^'.$2.'"]],'.
		     '["and",["=","name","operatingsystem"],["~","value","'.$1.'"]]'.
		     ']');
	    } elsif($sel =~ /^([*[:alpha:]]+)/){    # slc, rhel
		push(@ret,  '["and",["=","name","operatingsystem"],["~","value","'.$1.'"]]');
	    }  elsif($sel =~ /(\d+.*)$/) {            # *6
		push(@ret, '["and",["=","name","operatingsystemrelease"],["~","value","^'.$1.'"]]');
	    } else {
		$::this_app->warn("SSM::PuppetDB: did not understandd $type = $sel");
	    }
	} elsif ($type eq 'rack') {
	    push(@ret,
		   '["and",["=","name","landb_rackname"],["~","value","'.uc($sel).'"]]');
	} elsif ($type eq 'room') {
	    push(@ret,
		   '["and",["=","name","landb_location"],["~","value","'.uc($sel).'"]]');
	} elsif ( $type eq 'cluster' ) {
	    push(@ret,
                   '["and",["=","name","hostgroup"], ["~", "value","'.$sel.'"]]');
	} elsif ( $type eq 'stage' ) {
	    # for some reason "environment" is not a 'fact' like the others
	    if ( ($sel eq 'prod')) {  # legacy translations for "preprod" and "prod"
	      push(@ret,
		   '["=","environment","production"]]');
	      $::this_app->warn('SSM::PuppetDB: please specify puppet environment directly, do not use "prod" or "preprod"')
	    } elsif ( $sel eq 'preprod') {
	      push(@ret, '["=","environment","qa"]]');
	      $::this_app->warn('SSM::PuppetDB: please specify puppet environment directly, do not use "prod" or "preprod"')
	    } else {
	      push(@ret, '["=","environment","'.$sel.'"]]');
	    }
	} else {
	  # generic fact search with regex
	  push(@ret,
	       '["and",["=","name","'.$type.'"], ["~", "value","'.$sel.'"]]');
	}
    }
    if ($#ret > 0) {
      return '["or",'.join(",\n", @ret).']';
    } else {
      return $ret[0];
    }
}

1;

=pod 

=head1 Description

This SSM connects to the Puppetdb API via B<curl> (with Kerberos
authentication) and retrieves lists of hosts. Translates the various
query attributes into something that lookes like it might fit on
Puppetdb.

=over

=item I<stage=prod> becomes I<environment=production>,
I<stage=preprod> becomes I<environment=qa> - as long as either is the
sole argument. Otherwise just pass through.

=item the I<type> argument is abused to perform queries against
abitrary facts, by accepting a fact=value pair as argument

=back

=cut

=head1 Known Bugs

=over

=item may not handle Puppetdb errors gracefully (assumes anything
  coming back is JSON)

=item hardcoded to use https://judy.cern.ch

=back

=cut

=head1 SEE ALSO

wassh, ai-pdb

=head1 AUTHOR

Jan Iven
