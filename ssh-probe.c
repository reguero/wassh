/*
 * $Id: ssh-probe.c,v 1.1 2006/09/11 11:13:37 gcancio Exp $
 *
 * Description: connect to the SSH port to see if a host is active.
 * Author: David Hughes
 * Date: Jan 2003
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <netdb.h>
#include <getopt.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <netinet/in.h>

char *hostname;

#define BUFSZ 128
#define PROGRAM "ssh-probe"

void die(int perror, char *tag, char *fmt, ...);
#define fail(fmt, ...)   die(0, "FAIL",  fmt, ## __VA_ARGS__)
#define failp(fmt, ...)  die(1, "FAIL",  fmt, ## __VA_ARGS__)
#define error(fmt, ...)  die(0, "ERROR", fmt, ## __VA_ARGS__)
#define errorp(fmt, ...) die(1, "ERROR", fmt, ## __VA_ARGS__)

char *alarm_message;
void ALRM_handler(int sig) { fail(alarm_message); }

int main(int argc, char **argv)
{
	int sock;
	struct protoent *proto;
	struct hostent *host;
	struct servent *serv;
	struct sockaddr_in addr;
	FILE *ssh;
	char buf[BUFSZ];

	if (argc != 2) {
		fprintf(stderr, "usage: ssh-probe <hostname>\n");
		exit(1);
	}
	hostname = argv[1];

	signal(SIGALRM, ALRM_handler);

	if ((proto = getprotobyname("tcp")) == NULL)
		error("can't get protocol for 'tcp'");
	if ((serv = getservbyname("ssh","tcp")) == NULL)
		error("can't get service for 'ssh'/'tcp'");
	if ((host = gethostbyname(hostname)) == NULL) {
		error("No address associated with hostname");
	}
	if ((sock = socket(PF_INET, SOCK_STREAM, proto->p_proto)) == -1)
		errorp("can't open socket");
	addr.sin_family = AF_INET;
	addr.sin_port = serv->s_port;
	addr.sin_addr.s_addr = (in_addr_t)*(in_addr_t*)host->h_addr_list[0];
	alarm_message = "connect timeout";
	alarm(3);
	if ((connect(sock, (struct sockaddr *)&addr,
				sizeof(struct sockaddr_in)) == -1)) {
		fprintf(stderr, "FAIL ");
		perror(hostname);
		exit(1);
	}
	if (! (ssh = fdopen(sock, "r+"))) {
		errorp("fdopen");
	}
	alarm_message = "SSH daemon not responding";
	alarm(7);
	if (!fgets(buf, BUFSZ, ssh) || strncmp(buf, "SSH", 3)) {
		fail("ssh ident not received");
	}
	if (fprintf(ssh, "SSH-2.0-CERN_wassh\n") <= 0) {
		failp("can't send dummy SSH ident");
	}
	if (fclose(ssh)) failp("SSH error");
	return 0;
}

void die(int do_perror, char *tag, char *fmt, ...)
{
	va_list va;
	va_start(va,fmt);

	fprintf(stderr, "%s %s: ", tag, hostname);
	vfprintf(stderr, fmt, va);
	if (do_perror) {
		fprintf(stderr, ": ");
		perror("");
	} else {
		fprintf(stderr, "\n");
	}
	exit(1);
}
